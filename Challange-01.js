// Case 1
const changeWord = (selectedText, changedText, text) => {
  return text.replace(selectedText, changedText);
};

const kalimat1 = "Andini sangat mencintai kamu selamanya";
const kalimat2 = "Gunung bromo tak akan mampu menggambarkan besarnya cintaku padamu";
console.log("Case 1");
console.log(changeWord("mencintai", "membenci", kalimat1));
console.log(changeWord("bromo", "semeru", kalimat2));

// Case 2
const checkTypeNumber = (givenNumber) => {
  if (typeof givenNumber == "number") {
    return givenNumber % 2 == 0 ? "GENAP" : "GANJIL";
  } else if (givenNumber == undefined) {
    return "Error: Bro where is the parameter?";
  } else {
    return "Error: Invalid data type";
  }
};
console.log("Case 2");
console.log(checkTypeNumber(10));
console.log(checkTypeNumber(3));
console.log(checkTypeNumber("3"));
console.log(checkTypeNumber({}));
console.log(checkTypeNumber([]));
console.log(checkTypeNumber());

// Case 3
const checkEmail = (email) => {
  const pattern = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9-.]+\.[a-zA-Z]{2,6}$/;
  if (typeof email == "string" && email.includes("@")) {
    return pattern.test(email) ? "VALID" : "INVALID";
  } else {
    return "Error: Invalid data type";
  }
};
console.log("Case 3");
console.log(checkEmail("apranata@binar.co.id"));
console.log(checkEmail("apranata@binar.com"));
console.log(checkEmail("apranata@binar"));
console.log(checkEmail("apranata"));
console.log(checkTypeNumber(checkEmail(3322)));
console.log(checkEmail());

// Case 4
const isValidPassword = (givenPassword) => {
  pattern = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}$/;
  if (typeof givenPassword == "string") {
    return pattern.test(givenPassword) ? "TRUE" : "FALSE";
  } else {
    return "Error: Invalid data type";
  }
};
console.log("Case 4");
console.log(isValidPassword("Meong2021"));
console.log(isValidPassword("meong2021"));
console.log(isValidPassword("@eong"));
console.log(isValidPassword("Meong2"));
console.log(isValidPassword(0));
console.log(isValidPassword());

// Case 5
const getSplitName = (personName) => {
  if (typeof personName == "string") {
    const splitName = personName.split(" ");
    if (splitName.length < 4) {
      return {
        firstName: splitName[0],
        middleName: splitName[2] ? splitName[1] : null,
        lastName: splitName.length < 2 ? null : splitName[2] ? splitName[2] : splitName[1],
      };
    } else {
      return "This function is only for 3 characters name";
    }
  } else {
    return "Error: Invalid data type";
  }
};
console.log("Case 5");
console.log(getSplitName("Aldi Daniela Pranata"));
console.log(getSplitName("Dwi Kuncoro"));
console.log(getSplitName("Aurora"));
console.log(getSplitName("Aurora Aureliya Sukma Darma"));
console.log(getSplitName(0));

// Case 6
const getAngkaTerbesarKedua = (dataNumbers) => {
  if (typeof dataNumbers == "object" && !dataNumbers.some(isNaN)) {
    return dataNumbers.sort((a, b) => b - a)[1];
  } else {
    return "Error: Invalid data type";
  }
};
const dataAngka = [9, 4, 7, 7, 4, 3, 2, 2, 8];
console.log("Case 6");
console.log(getAngkaTerbesarKedua(dataAngka));
console.log(getAngkaTerbesarKedua(0));
console.log(getAngkaTerbesarKedua());

// Case 7
const dataPenjualanPakAdi = [
  {
    namaProduct: "Sepatu Futsal Nike Vapor Academy 8",
    hargaSatuan: 760000,
    kategori: "Sepatu Sport",
    totalTerjual: 90,
  },
  {
    namaProduct: "Sepatu Warrior Tristan Black Brown High",
    hargaSatuan: 960000,
    kategori: "Sepatu Sneaker",
    totalTerjual: 37,
  },
  {
    namaProduct: "Sepatu Warrior Tristan Maroon High",
    hargaSatuan: 360000,
    kategori: "Sepatu Sneaker",
    totalTerjual: 90,
  },
  {
    namaProduct: "Sepatu Warrior Rainbow Tosca Corduroy",
    hargaSatuan: 120000,
    kategori: "Sepatu Sneaker",
    totalTerjual: 90,
  },
];

const getTotalPenjualan = (dataPenjualan) => {
  const objectValidasi = dataPenjualan.every((element) => typeof element == "object");
  if (typeof dataPenjualan == "object" && objectValidasi) {
    // Cara 1
    return dataPenjualan.reduce((prev, { totalTerjual }) => prev + totalTerjual, 0);
    // Cara 2
    // let jumlahTotalTerjual = 0;
    // dataPenjualan.forEach( data => jumlahTotalTerjual += data.totalTerjual);
    // return jumlahTotalTerjual;
  } else {
    return "Error: Invalid data type";
  }
};
console.log("Case 7");
console.log(getTotalPenjualan(dataPenjualanPakAdi));

// case 8
const dataPenjualNovel = [
  {
    idProduct: "BOOK002421",
    namaProduct: "Pulang - Pergi",
    penulis: "Tere Liye",
    hargaBeli: 60000,
    hargaJual: 86000,
    totalTerjual: 150,
    sisaStok: 17,
  },
  {
    idProduct: "BOOK002351",
    namaProduct: "Selamat Tinggal",
    penulis: "Tere Liye",
    hargaBeli: 75000,
    hargaJual: 103000,
    totalTerjual: 171,
    sisaStok: 20,
  },
  {
    idProduct: "BOOK002941",
    namaProduct: "Garis Waktu",
    penulis: "Fiersa Besari",
    hargaBeli: 67000,
    hargaJual: 99000,
    totalTerjual: 213,
    sisaStok: 5,
  },
  {
    idProduct: "BOOK002941",
    namaProduct: "Laskar Pelangi",
    penulis: "Andrea Hirata",
    hargaBeli: 55000,
    hargaJual: 68000,
    totalTerjual: 20,
    sisaStok: 56,
  },
];
const getInfoPenjualan = (dataPenjualan) => {
  const objectValidasi = dataPenjualan.every((e) => typeof e == "object");
  if (typeof dataPenjualan == "object" && objectValidasi) {
    const rupiah = (number) => {
      return new Intl.NumberFormat("id-ID", {
        style: "currency",
        currency: "IDR",
      }).format(number);
    };
    // Cara 1
    let totalModal = dataPenjualan.reduce((modal, e) => modal += e.hargaBeli * (e.totalTerjual + e.sisaStok), 0)
    // Cara 2
    // const modal = dataPenjualan.map(e => e.hargaBeli * (e.totalTerjual + e.sisaStok))
    // let totalModal = 0
    // modal.forEach( data => totalModal += data)

    // Cara 1
    let totalPendapatan = dataPenjualan.reduce((pendapatan, e) => pendapatan += e.hargaJual * (e.totalTerjual), 0)
    // Cara 2
    // const pendapatan = dataPenjualan.map(e => e.hargaJual * (e.totalTerjual))
    // let totalPendapatan = 0
    // pendapatan.forEach( data => totalPendapatan += data)

    let totalKeuntungan = totalPendapatan - totalModal;

    let presentaseKeuntungan = (totalKeuntungan / totalModal) * 100;

    let terlaris = Math.max.apply(
      Math,
      dataPenjualan.map((o) => o.totalTerjual)
    );
    let bukuTerlaris = dataPenjualan.filter((e) => e.totalTerjual == terlaris);
    return {
      totalKeuntungan: rupiah(totalKeuntungan),
      totalModal: rupiah(totalModal),
      presentaseKeuntungan: Math.round(presentaseKeuntungan) + "%",
      produkBukuTerlaris: bukuTerlaris[0].namaProduct,
      penulisTerlaris: bukuTerlaris[0].penulis,
    };
  } else {
    return "Error: Invalid data type";
  }
};
console.log("Case 8");
console.log(getInfoPenjualan(dataPenjualNovel));
